#include <curses.h>
#include <iostream>
#include <fstream>
#include <string.h>
#include "g_database.hpp"
#include "wordle.hpp"
#include <random>


static void	invalidInput(WINDOW *win_ptr, std::string const &word, int y)
{
	wmove(win_ptr, y, 1);
	for (int i = 0 ; i < WORD_LEN ; i++)
	{
		waddch(win_ptr, std::toupper(word[i]) | A_UNDERLINE);
		if (i == WORD_LEN - 1)
			waddch(win_ptr, '\n');
		else
			waddch(win_ptr, ' ');
	}
}

std::string	getWord(int y, WINDOW *win_ptr, std::string const &previousWord)
{
	int		ret;
	char	word[6];

	for (int i = 0 ; i < 6; i++)
		word[i] = previousWord.c_str()[i];

	word[5] = 0;

	wattron(win_ptr, COLOR_PAIR(DEFAULT));
	for (int i=0; i < WORD_LEN; i++)
	{
		wmove(win_ptr, y, 1 + 2 * i);
		refresh();
		ret = wgetch(win_ptr);
		if (ret == KEY_LEFT && i > 0)
			i -= 2;
		else if (ret == KEY_BACKSPACE)
		{
			if (word[i])
			{
				waddch(win_ptr, '_' | COLOR_PAIR(NOT_FOUND));
				word[i] = 0;
			}
			else if (i > 0)
			{
				i--;
				wmove(win_ptr, y, 1 + 2 * i);
				word[i] = 0;
				waddch(win_ptr, '_' | COLOR_PAIR(NOT_FOUND));
				refresh();
			}
			i--;
		}
		else if (ret == KEY_RIGHT && i < WORD_LEN - 1)
			;
		else if ((ret == KEY_ENTER || ret == '\n') && strlen(word) >= WORD_LEN)
			break;
		else if ((!std::isalpha(ret) || ret == EOF))
			i--;
		else if (std::isalpha(ret))
		{
			waddch(win_ptr, std::toupper(ret) | A_UNDERLINE);
			word[i] = std::tolower(ret);
			if (i == WORD_LEN - 1)
				i--;
		}
	}
	wattroff(win_ptr, COLOR_PAIR(DEFAULT));
	return (word);
}

inline static std::string const	&__randomWordPick(void)
{
	uint const	rng = std::random_device()() % g_database.size();
	auto		iter = g_database.begin();

	std::advance(iter, rng);
	return *iter;
}

static void	__loStr(std::string &str)
{
	for (std::size_t i = 0ULL; i < str.size() ; i++)
		str[i] = std::tolower(str[i]);
}

#define HEIGHT 15
#define WIDTH 11

int main(void)
{
	WINDOW		*win_ptr;
	std::string	toGuess = __randomWordPick();

	initscr();
	start_color();
	noecho();

	init_color(COLOR_GREY, 500, 500, 500);
	init_pair(NOT_FOUND, COLOR_GREY, COLOR_BLACK);
	init_pair(WRONG_PLACE, COLOR_YELLOW, COLOR_BLACK);
	init_pair(WELL_PLACED, COLOR_GREEN, COLOR_BLACK);
	init_pair(INVALID, COLOR_RED, COLOR_BLACK);

	win_ptr = newwin(HEIGHT, WIDTH, 0, 0);

	keypad(win_ptr, TRUE);

	wattron(win_ptr, COLOR_PAIR(NOT_FOUND));
	for (int i = 0 ; i < MAX_GUESS ; i++)
	{
		wprintw(win_ptr, " _ _ _ _ _\n\n");
	}
	wattroff(win_ptr, COLOR_PAIR(NOT_FOUND));

	std::string userGuess;
	bool	isInvalid = false;
	int		i;
	for (i=0; i < MAX_GUESS; i++)
	{
		if (!isInvalid)
			userGuess = getWord(i * 2, win_ptr, "\0\0\0\0\0");
		else
			userGuess = getWord(i * 2, win_ptr, userGuess);
		__loStr(userGuess);
		if (g_database.find(userGuess) == g_database.end())
		{
			wattron(win_ptr, COLOR_PAIR(INVALID));
			invalidInput(win_ptr, userGuess, i * 2);
			wattroff(win_ptr, COLOR_PAIR(INVALID));
			i--;
			isInvalid = true;
		}
		else
		{
			wmove(win_ptr, i*2, 1);
			refresh();
			curs_set(0);
			printWord(userGuess, toGuess, win_ptr);
			curs_set(1);
			isInvalid = false;
			if (userGuess == toGuess)
				break;
		}
	}
	if (i == MAX_GUESS)
	{
		wmove(win_ptr, i*2, 1);
		refresh();
		curs_set(0);
		wattron(win_ptr, A_BOLD);
		curs_set(0);
		printWord(toGuess, toGuess, win_ptr);
		curs_set(1);
		wattroff(win_ptr, A_BOLD);
	}

	wgetch(win_ptr);

	endwin();
	return 0;
}
