#include <curses.h>
#include <string>
#include <iostream>
#include <unistd.h>
#include "wordle.hpp"

#ifndef MAX_TRY_COUNT
# define MAX_TRY_COUNT	6U
#endif

typedef unsigned char	hhuint;
typedef	unsigned int	uint;

/**
	@brief Counts the number of time c appears in str
*/
inline static hhuint	__countChar(std::string const &str, char const c)
{
	hhuint	count;
	hhuint	idx;

	count = 0U;
	for (idx = 0U ; idx < 5U ; ++idx)
		if (str[idx] == c)
			++count;
	return count;
}

/**
	@brief Counts the number of time c appears between str[0] and str[end]
*/
inline static hhuint	__countChar(
	std::string const &str,
	char const c,
	hhuint const end)
{
	hhuint	count;
	hhuint	idx;

	count = 0U;
	for (idx = 0U ; idx <= end ; ++idx)
		if (str[idx] == c)
			++count;
	return (count);
}

inline static hhuint	__commonMatch(
	std::string const &str1,
	std::string const &str2,
	char const c)
{
	hhuint	count;
	hhuint	idx;

	count = 0U;
	for (idx = 0 ; idx < 5U ; ++idx)
		if (str1[idx] == c && str2[idx] == c)
			++count;
	return (count);
}

/**
	@brief Displays the result of the guess entered by the player

	@warning userGuess should be a valid input (present in the dictionnary)

	@param userGuess User's proposition
	@param toGuess Target word

	@return If the user guessed the word successfuly, true is returned.
			Otherwise, false is returned.
*/
void	printWord(
	std::string const &userGuess,
	std::string const &toGuess,
	WINDOW *win_ptr)
{
	hhuint	idx;

	for (idx = 0U ; idx < 5U ; idx++)
	{
		hhuint	commonMatch = __commonMatch(userGuess, toGuess, userGuess[idx]);
		hhuint	nbInUser = __countChar(userGuess, idx, userGuess[idx]);
		hhuint	nbInGuess = __countChar(toGuess, userGuess[idx]);
		hhuint	colorCode;
		if (userGuess[idx] == toGuess[idx])
			colorCode = WELL_PLACED;
		else if (nbInUser - commonMatch <= nbInGuess - commonMatch &&
				nbInGuess - commonMatch != 0)
			colorCode = WRONG_PLACE;
		else
			colorCode = NOT_FOUND;
		wattron(win_ptr, COLOR_PAIR(colorCode));
		waddch(win_ptr, std::toupper(userGuess[idx]) | A_UNDERLINE);
		wattroff(win_ptr, COLOR_PAIR(colorCode));
		if (idx != 4)
		{
			waddch(win_ptr, ' ');
			wrefresh(win_ptr);
			usleep(500000);
		}
	}
}
