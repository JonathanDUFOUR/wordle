#ifndef DATA_HPP
# define DATA_HPP

#define DEFAULT 0
#define NOT_FOUND 1
#define WRONG_PLACE 2
#define WELL_PLACED 3
#define INVALID 4

#define COLOR_GREY 8

#define WORD_LEN 5

#define MAX_GUESS 6
void	printWord(std::string const &userGuess, std::string const &toGuess, WINDOW *win_ptr);

#endif /* DATA_HPP */
