/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   colors.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jodufour <jodufour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/02/19 18:02:08 by hperrin           #+#    #+#             */
/*   Updated: 2022/05/14 18:42:13 by jodufour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef COLORS_HPP
# define COLORS_HPP

# define CLEAR_SCREEN	"\033[2J\33[0;0H"

# define RESET_COLOR	"\033[0m"
# define BOLD_RED		"\033[38;2;255;0;0;1m"
# define GREEN			"\033[38;2;0;255;0m"
# define YELLOW			"\033[38;2;255;255;0m"
# define GREY			"\033[38;2;128;128;128m"

#endif
